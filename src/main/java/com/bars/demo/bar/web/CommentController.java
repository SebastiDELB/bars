package com.bars.demo.bar.web;

import com.bars.demo.bar.model.Comment;
import com.bars.demo.bar.service.CommentService;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;


@Controller
@SpringBootApplication
@RestController
@RequestMapping("/comment")
@ResponseBody
public class CommentController implements CommentService {

    Comment comment = new Comment();

    @RequestMapping(value = "/{commentId}",method = GET)
    public Comment getCommentById(@PathVariable final Integer commentId){
        comment.setId(commentId);
        return comment;
    }

    @RequestMapping(value = "/barId/{barId}",method = GET)
    public Comment getCommentByBar(@PathVariable final String barId){
        comment.setBar_id(barId);
        return comment;
    }


    @RequestMapping(value = "/add",method = POST)
    public void addComment(@RequestBody Comment comment){
        //this.
    }

    @Override
    public List<Comment> getAllComment() {
        return null;
    }
}
