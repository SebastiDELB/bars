package com.bars.demo.bar.web;


import com.bars.demo.bar.model.Friend;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@SpringBootApplication
@RestController
@RequestMapping("/friend/{friendId}")
@ResponseBody
public class FriendController {

    Friend friend = new Friend();

    @RequestMapping(method = GET)
    public Friend getFriendById(@PathVariable final Integer friendId){
        friend.setId(friendId);
        return friend;
    }
}
