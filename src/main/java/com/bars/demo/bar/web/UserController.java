package com.bars.demo.bar.web;

import com.bars.demo.bar.model.User;
import com.bars.demo.bar.service.UserService;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@SpringBootApplication
@RestController
@RequestMapping("/user")
@ResponseBody
public class UserController {


    private UserService userService;

/**

    public  UserController(final UserService userService){
        this.userService = userService;
    }
*/
    User user = new User();

    @RequestMapping(method = RequestMethod.GET)
    public User getAllUser() {
        return user; //should return a List<>
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public User returnUserById(@PathVariable Integer userId) {
        user.setId(userId);
        return user;
    }


}

