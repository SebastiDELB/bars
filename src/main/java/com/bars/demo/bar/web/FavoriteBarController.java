package com.bars.demo.bar.web;

import com.bars.demo.bar.model.FavoriteBar;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@SpringBootApplication
@RestController
@RequestMapping("/favoriteBar/{favorite_bar_id}")
public class FavoriteBarController {

    FavoriteBar favoriteBar = new FavoriteBar();

    @RequestMapping(method = GET)
    public FavoriteBar returnBarById(@PathVariable final Integer favorite_bar_id)
    {
        favoriteBar.setId(favorite_bar_id);
        return favoriteBar;
    }

}
