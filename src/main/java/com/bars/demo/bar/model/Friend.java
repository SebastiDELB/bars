package com.bars.demo.bar.model;

import com.bars.demo.bar.Model;
import lombok.Getter;
import lombok.ToString;


@ToString
@Getter
public class Friend extends Model {

    public int friend_id;
    public int user_id;
    public int id;

    public int getFriend_id() {
        return friend_id;
    }

    public void setFriend_id(int friend_id) {
        this.friend_id = friend_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
