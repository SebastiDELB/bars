package com.bars.demo.bar.model;

import com.bars.demo.bar.Model;
import lombok.Getter;
import lombok.ToString;


@ToString
@Getter
public class Comment extends Model {

    public int id;
    public String content;
    public String bar_id;
    public int user_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getBar_id() {
        return bar_id;
    }

    public void setBar_id(String bar_id) {
        this.bar_id = bar_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
