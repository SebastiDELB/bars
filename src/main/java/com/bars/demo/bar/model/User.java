package com.bars.demo.bar.model;

import com.bars.demo.bar.Model;
import lombok.Getter;
import lombok.ToString;


@ToString
@Getter

public class User extends Model {

    public String pseudo;
    public String password;
    public int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
