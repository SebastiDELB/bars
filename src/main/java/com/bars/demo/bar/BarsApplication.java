package com.bars.demo.bar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;


@SpringBootApplication
public class BarsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BarsApplication.class, args);

        //https://www.getpostman.com/collections/802cc888aaa2085cd33d


    }

} 
