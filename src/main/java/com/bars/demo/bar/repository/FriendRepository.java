package com.bars.demo.bar.repository;

import com.bars.demo.bar.model.Friend;
import java.util.List;

public interface FriendRepository {

    Friend getFriendById(Integer FriendId);

    void adFriend(Friend friend);

    List<Friend> getAllFriend();
}
