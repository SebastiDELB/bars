package com.bars.demo.bar.repository;

import com.bars.demo.bar.model.FavoriteBar;
import java.util.List;

public interface FavoriteBarRepository {

    FavoriteBar getFavoriteBarById(Integer favoriteBarId);

    void addFavoriteBar(FavoriteBar favoriteBar);

    List<FavoriteBar> getAllFavoriteBar();

}
