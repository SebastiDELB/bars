package com.bars.demo.bar.repository;

import com.bars.demo.bar.model.Comment;
import java.util.List;

public interface CommentRepository {

    Comment getCommentById(Integer CommentId);

    void addComment(Comment comment);

    List<Comment> getAllComment();
}
