package com.bars.demo.bar.repository;

import com.bars.demo.bar.model.User;

import java.util.List;

public interface UserRepository{

        User getUserById(Integer UserId);

        void addUser(User user);

        List<User> getAllUser();
}
