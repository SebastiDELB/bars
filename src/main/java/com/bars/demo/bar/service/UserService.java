package com.bars.demo.bar.service;

import com.bars.demo.bar.model.User;

import java.util.List;

public interface UserService {

    User getUserById(Integer UserId);

    void addUser(User user);

    List<User> getAllUser();
}
