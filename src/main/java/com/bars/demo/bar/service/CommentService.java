package com.bars.demo.bar.service;

import com.bars.demo.bar.model.Comment;
import com.bars.demo.bar.repository.CommentRepository;

import java.util.List;

public interface CommentService extends CommentRepository {

    Comment getCommentById(Integer commentId);

    void addComment(Comment comment);

    List<Comment> getAllComment();
}
