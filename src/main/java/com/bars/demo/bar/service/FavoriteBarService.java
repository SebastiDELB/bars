package com.bars.demo.bar.service;

import com.bars.demo.bar.model.FavoriteBar;

import java.util.List;

public interface FavoriteBarService {

    FavoriteBar getFavoriteBarById(Integer favoriteBarId);

    void addFavoriteBar(FavoriteBar favoriteBar);

    List<FavoriteBar> getAllFavoriteBar();

}